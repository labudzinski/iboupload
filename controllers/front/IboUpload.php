<?php
/**
* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt. 
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*/

class IboUploadIboUploadModuleFrontController extends ModuleFrontController
{
    public $auth = true;
    public $ssl = true;

    public function setMedia()
    {
        parent::setMedia();
        $this->addCSS(_THEME_CSS_DIR_.'my-account.css');
    }
    /**
     * Assign template vars related to page content
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $has_address = $this->context->customer->getAddresses($this->context->language->id);
        $this->context->smarty->assign(array(
            'has_customer_an_address' => empty($has_address),
            'voucherAllowed' => (int)CartRule::isFeatureActive(),
            'returnAllowed' => (int)Configuration::get('PS_ORDER_RETURN')
        ));
        $this->context->smarty->assign('HOOK_CUSTOMER_ACCOUNT', Hook::exec('displayCustomerAccount'));

        $this->setTemplate('upload.tpl');
    }
}
