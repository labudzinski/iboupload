<?php
/**
 * 2015 Dominik Labudzinski
 *
 * NOTICE OF LICENSE
 *
 * Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-Design Dominik Labudzinski.
 * Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
 * (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
 * Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
 * użytkownik zgadza się przestrzegać warunki licencji.
 * Pełny tekst warunków gwarancji i licencji zawiera produkt.
 *
 *  @author    Dominik Labudzinski <dominik@labudzinski.com>
 *  @copyright 2015 Dominik Labudzinski
 *  @license   Commercial
 */

class AdminIboUploadFilesController extends AdminControllerCore
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->lang = false;
        $this->table = 'order_upload';
        $this->explicitSelect = true;
        $this->_defaultOrderBy = 'id';
        $this->identifier = 'id';
        $this->toolbar_title = $this->l('Customer files uploader');
        $this->tabAccess['add'] = '0';
        $this->context = Context::getContext();
        $this->list_simple_header = false;

        $this->fields_list = array(
            'id' => array(
                'title' => $this->l('ID'),
                'align' => 'center',
                'class' => 'fixed-width-xs'
            ),
            'file_name' => array(
                'title' => $this->l('File name'),
                'havingFilter' => true,
            ),
            'ext' => array(
                'title' => $this->l('File extension'),
                'width' => 150,
            ),
            'order' => array(
                'title' => $this->l('Order'),
                'width' => 150,
                'filter_key' => 'o!reference'
            ),
            'product' => array(
                'title' => $this->l('Product'),
                'width' => 150,
                'filter_key' => 'c!name'
            ),
        );
        $this->_select .= 'file_name, SUBSTRING_INDEX(file_name,\'.\',-1) ext';
        $this->_join = 'JOIN '._DB_PREFIX_.'product_lang c ON (a.id_product = c.id_product)';
        $this->_join .= 'JOIN '._DB_PREFIX_.'order_detail oi ON (a.id_order_detail = oi.id_order_detail)';
        $this->_join .= 'JOIN '._DB_PREFIX_.'orders o ON (oi.id_order = o.id_order)';
        $this->_where = 'AND c.id_lang = '.(int)$this->context->language->id;
        parent::__construct();

        $this->_use_found_rows = false;
    }

    public function initPageHeaderToolbar()
    {
        $this->page_header_toolbar_btn['new_customer'] = array(
            'href' => 'index.php?controller=AdminModules&configure=iboupload&tab_module=front_office_features&module_name=iboupload&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Settings', null, null, false),
            'icon' => 'process-icon-modules-list'
        );

        parent::initPageHeaderToolbar();
    }

    public function renderView()
    {
        $id = (int)Tools::getValue('id');
        $orderUpload  = Db::getInstance()->getRow('SELECT file_path, file_name,
            SUBSTRING_INDEX(file_name,\'.\',-1) AS ext FROM '._DB_PREFIX_.'order_upload
			WHERE id = '.$id);
        $filePath = $orderUpload['file_path'];
        $fileName = $orderUpload['file_name'];
        $ext = $orderUpload['ext'];
        if ($filePath && file_exists($filePath)) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $filePath);
            finfo_close($finfo);
            header('Content-Type: '.$mime);
            header('Content-Length: '.filesize($filePath));
            header('Content-Disposition: filename='.$fileName.'.'.$ext);
            readfile($filePath);
        }

        Tools::redirectAdmin(self::$currentIndex.'&token='.$this->token);

    }

    public function renderList()
    {
        $this->_use_found_rows = false;
        $this->addRowAction('view');
        //$this->addRowAction('delete');
        $this->bulk_actions = array(
            'delete' => array(
                'text' => $this->l('Delete selected'),
                'confirm' => $this->l('Delete selected files?'),
                'icon' => 'icon-trash'
            )
        );

        return parent::renderList();
    }

    public function initToolbar()
    {
        parent::initToolbar();
        unset($this->toolbar_btn['new']);
    }

    public function getList($id_lang, $order_by = null, $order_way = null, $start = 0, $limit = null, $id_lang_shop = false)
    {
        parent::getList($id_lang, $order_by, $order_way, $start, $limit, Context::getContext()->shop->id);
    }

    public function initContent()
    {
        parent::initContent();
    }
}
