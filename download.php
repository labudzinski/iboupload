<?php
/**
* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt. 
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*/

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache');
set_time_limit(5 * 60);
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');
require_once(dirname(__FILE__).'/iboupload.php');

$upload = new Iboupload();
if (Tools::isSubmit('download')) {
    $files = array();
    $downloads = array();
    $order = new Order((int)Tools::getValue('id_order'));
    $details = $order->getOrderDetailList();
    foreach ($details as $detail) {
        $result = Db::getInstance()->executeS('SELECT *
FROM '._DB_PREFIX_.'order_upload
WHERE id_order_detail = '.$detail['id_order_detail']);
        foreach ($result as $item) {
            $product_attribute_string = array();
            $url = str_replace(_PS_UPLOAD_DIR_, '', $item['file_path']);
            if (Tools::file_exists_no_cache(_PS_UPLOAD_DIR_.$url)) {
                $product = new ProductCore($item['id_product']);
                $attributtes = ProductCore::getAttributesParams($item['id_product'], $item['id_product_attribute']);
                $product_attribute_string[] = $product->name[Context::getContext()->language->id];
                foreach ($attributtes as $attributte) {
                    $product_attribute_string[] = $attributte['group'].': '.$attributte['name'];
                }
                $downloads[$item['id_product'].'_'.$item['id_product_attribute']][] = _PS_UPLOAD_DIR_.$url;
            }
        }
    }

    if (!$downloads) {
        return;
    }

    $zipname = _PS_DOWNLOAD_DIR_.$order->reference.'.zip';
    require_once(realpath(dirname(__FILE__)).'/zip.php');
    $zip = new Zip();
    foreach ($downloads as $key => $download) {
        $product_data = explode('_', $key);
        $product_attribute_string = array();

        $product = new Product($product_data[0]);
        $attributtes = Product::getAttributesParams($product_data[0], $product_data[1]);
        $product_attribute_string[] = $product->name[Context::getContext()->language->id];
        foreach ($attributtes as $attributte) {
            $product_attribute_string[] = $attributte['group'].': '.$attributte['name'];
        }

        foreach ($download as $file) {
            $file_name = str_replace(_PS_UPLOAD_DIR_.'iboupload/', '', $file);
            $zip->addFile(fopen($file, 'r'), implode('/', $product_attribute_string).'/'.$file_name);

        }
    }
    $zip->finalize();
    $zip->setZipFile($zipname);
    //Send zip folder
    header('Content-Type: application/zip');
    header('Content-Length: '.filesize($zipname));
    header('Content-Disposition: filename='.$order->reference.'.zip');
    readfile($zipname);
}
exit;
