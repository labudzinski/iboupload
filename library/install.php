<?php
/**
 * 2014 Dominik Labudzinski
 *
 * NOTICE OF LICENSE
 *
 * Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-Design Dominik Labudzinski.
 * Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
 * (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
 * Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-Design Dominik Labudzinski
 * użytkownik zgadza się przestrzegać warunki licencji. Pełny tekst warunków gwarancji i licencji zawiera produkt.
 *
 *  @author    Dominik Labudzinski <dominik@labudzinski.com>
 *  @copyright 2014 Dominik Labudzinski
 *  @license   Commercial
 */

class Install
{
    /**
     * @return bool
     */
    public static function installDB()
    {
        $sql = array();
        $sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'cart_upload (
		  id bigint(20) NOT NULL AUTO_INCREMENT,
		  id_product int(10) NOT NULL,
		  id_product_attribute int(10) NOT NULL,
		  id_cart int(10) NOT NULL,
		  file_path varchar(1000) NOT NULL,
		  file_hash varchar(32) NOT NULL,
		  file_name varchar(400) NOT NULL,
		  info TEXT NULL,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';
        $sql[] = 'CREATE TABLE IF NOT EXISTS '._DB_PREFIX_.'order_upload (
		  id bigint(20) NOT NULL AUTO_INCREMENT,
		  id_order_detail int(10) NOT NULL,
		  id_product_attribute int(10) NOT NULL,
		  id_product int(10) NOT NULL,
		  file_path varchar(1000) NOT NULL,
		  file_hash varchar(32) NOT NULL,
		  file_name varchar(400) NOT NULL,
		  info TEXT NULL,
		  PRIMARY KEY (id)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;';

        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product ADD can_upload TINYINT(1) NOT NULL DEFAULT 0';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product ADD multiupload TINYINT(1) NOT NULL DEFAULT 0';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product ADD maximum_files INT(10) NOT NULL DEFAULT 0';

        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute ADD can_upload TINYINT(1) NOT NULL DEFAULT 0';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute ADD multiupload TINYINT(1) NOT NULL DEFAULT 0';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute ADD maximum_files INT(10) NOT NULL DEFAULT 0';

        foreach ($sql as $query) {
            if (! Db::getInstance()->Execute($query)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public static function uninstallDB()
    {
        $sql = array();
        $sql[] = 'DROP TABLE '._DB_PREFIX_.'cart_upload';
        $sql[] = 'DROP TABLE '._DB_PREFIX_.'order_upload';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product DROP COLUMN can_upload';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product DROP COLUMN multiupload';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product DROP COLUMN maximum_files';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute DROP COLUMN can_upload';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute DROP COLUMN multiupload';
        $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute DROP COLUMN maximum_files';

        foreach ($sql as $query) {
            if (! Db::getInstance()->Execute($query)) {
                return false;
            }
        }

        return true;
    }

    public static function installMenu()
    {
        $insert = array(
            'en' => "Customer files",
            'gb' => "Customer files",
            'fr' => "Les fichiers des clients",
            'de' => "Kunden Dateien",
            'pl' => "Pliki klientów"
        );
        $db = Db::getInstance();
        $inserted = $db->insert(
            'tab',
            array(
                'id_parent' => 0,
                'class_name' => 'AdminIboUploadFiles',
                'module' => 'iboupload',
                'position' => 1,
                'active' => 1
            )
        );
        if ($inserted === false) {
            return false;
        }

        $tab = Db::getInstance()->getValue('SELECT id_tab from '._DB_PREFIX_.'tab WHERE class_name = "AdminIboUploadFiles"');
        foreach ($insert as $k => $lang) {
            $idByIso = Language::getIdByIso($k);
            if ($idByIso) {
                $inserted = $db->insert(
                    'tab_lang',
                    array(
                        'id_tab' => $tab,
                        'id_lang' => $idByIso,
                        'name' => $lang
                    )
                );
                if ($inserted === false) {
                    return false;
                }
            }
        }
        return true;
    }

    public static function uninstallMenu()
    {
        $sql = array();
        $sql[] = 'DELETE FROM '._DB_PREFIX_.'tab WHERE module = "iboupload"';

        foreach ($sql as $query) {
            if (! Db::getInstance()->Execute($query)) {
                return false;
            }
        }

        return true;
    }
}
