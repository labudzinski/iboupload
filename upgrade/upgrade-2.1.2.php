<?php
/**
* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt. 
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_1_2($object)
{
    $insert = array(
        'en' => "Customer files",
        'gb' => "Customer files",
        'fr' => "Les fichiers des clients",
        'de' => "Kunden Dateien",
        'pl' => "Pliki klientów"
    );
    $db = Db::getInstance();
    $inserted = $db->insert(
        'tab',
        array(
            'id_parent' => 0,
            'class_name' => 'AdminIboUploadFiles',
            'module' => 'iboupload',
            'position' => 1,
            'active' => 1
        )
    );
    if ($inserted === false) {
        return false;
    }

    $tab = Db::getInstance()->getValue('SELECT id_tab from '._DB_PREFIX_.'tab WHERE class_name = "AdminIboUploadFiles"');
    foreach ($insert as $k => $lang) {
        $idByIso = Language::getIdByIso($k);
        if ($idByIso) {
            $inserted = $db->insert(
                'tab_lang',
                array(
                    'id_tab' => $tab,
                    'id_lang' => $idByIso,
                    'name' => $lang
                )
            );
            if ($inserted === false) {
                return false;
            }
        }
    }
    return true;
}
