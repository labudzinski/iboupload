<?php
/**
* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt. 
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

function upgrade_module_2_1($object)
{
    $sql = array();
    $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute ADD can_upload TINYINT(1) NOT NULL DEFAULT 0';
    $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute ADD multiupload TINYINT(1) NOT NULL DEFAULT 0';
    $sql[] = 'ALTER TABLE '._DB_PREFIX_.'product_attribute ADD maximum_files INT(10) NOT NULL DEFAULT 0';

    $execute = Db::getInstance()->Execute(implode(";", $sql));
    if (!$execute) {
        return false;
    }
    return true;
}
