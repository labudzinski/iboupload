<?php
/**
 * 2014 Dominik Labudzinski
 *
 * NOTICE OF LICENSE
 *
 * Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
 * Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
 * (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
 * Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
 * użytkownik zgadza się przestrzegać warunki licencji.
 * Pełny tekst warunków gwarancji i licencji zawiera produkt.
 *
 *  @author    Dominik Labudzinski <dominik@labudzinski.com>
 *  @copyright 2014 Dominik Labudzinski
 *  @license   Commercial
 */

class Zip
{
    const VERSION = 1.62;

    const ZIP_LOCAL_FILE_HEADER = "\x50\x4b\x03\x04";

    const ZIP_CENTRAL_FILE_HEADER = "\x50\x4b\x01\x02";

    const ZIP_END_OF_CENTRAL_DIRECTORY = "\x50\x4b\x05\x06\x00\x00\x00\x00";

    const EXT_FILE_ATTR_DIR = 010173200020;

    const EXT_FILE_ATTR_FILE = 020151000040;

    const ATTR_VERSION_TO_EXTRACT = "\x14\x00";

    const ATTR_MADE_BY_VERSION = "\x1E\x03";

    const EXTRA_FIELD_NEW_UNIX_GUID = "\x75\x78\x0B\x00\x01\x04\xE8\x03\x00\x00\x04\x00\x00\x00\x00";

    const S_IFIFO = 0010000;

    const S_IFCHR = 0020000;

    const S_IFDIR = 0040000;

    const S_IFBLK = 0060000;

    const S_IFREG = 0100000;

    const S_IFLNK = 0120000;

    const S_IFSOCK = 0140000;

    const S_ISUID = 0004000;

    const S_ISGID = 0002000;

    const S_ISTXT = 0001000;

    const S_IRWXU = 0000700;

    const S_IRUSR = 0000400;

    const S_IWUSR = 0000200;

    const S_IXUSR = 0000100;

    const S_IRWXG = 0000070;

    const S_IRGRP = 0000040;

    const S_IWGRP = 0000020;

    const S_IXGRP = 0000010;

    const S_IRWXO = 0000007;

    const S_IROTH = 0000004;

    const S_IWOTH = 0000002;

    const S_IXOTH = 0000001;

    const S_ISVTX = 0001000;

    const S_DOS_A = 0000040;

    const S_DOS_D = 0000020;

    const S_DOS_V = 0000010;

    const S_DOS_S = 0000004;

    const S_DOS_H = 0000002;

    const S_DOS_R = 0000001;

    private $zipMemoryThreshold = 1048576;

    private $zipData = null;

    private $zipFile = null;

    private $zipComment = null;

    private $cdRec = array();

    private $offset = 0;

    private $isFinalized = false;

    private $addExtraField = true;

    private $streamChunkSize = 65536;

    private $streamFilePath = null;

    private $streamTimestamp = null;

    private $streamFileComment = null;

    private $streamFile = null;

    private $streamData = null;

    private $streamFileLength = 0;

    private $streamExtFileAttr = null;

    /**
     * A custom temporary folder, or a callable that returns a custom temporary file.
     *
     * @var string callable
     */
    public static $temp = null;

    /**
     * Constructor.
     *
     * @param boolean $useZipFile
     *            Write temp zip data to tempFile? Default false
     */
    public function __construct($useZipFile = false)
    {
        if ($useZipFile) {
            $this->zipFile = tmpfile();
        } else {
            $this->zipData = "";
        }
    }

    public function __destruct()
    {
        if (is_resource($this->zipFile)) {
            fclose($this->zipFile);
        }

        $this->zipData = null;
    }

    /**
     * Extra fields on the Zip directory records are Unix time codes needed for compatibility on the
     * default Mac zip archive tool.
     * These are enabled as default, as they do no harm elsewhere and only add 26 bytes per file added.
     *
     * @param bool $setExtraField
     *            true (default) will enable adding of extra fields, anything else will disable it.
     */
    public function setExtraField($setExtraField = true)
    {
        $this->addExtraField = ($setExtraField === true);
    }

    /**
     * Set Zip archive comment.
     *
     * @param string $newComment
     *            New comment. null to clear.
     * @return bool $success
     */
    public function setComment($newComment = null)
    {
        if ($this->isFinalized) {
            return false;
        }

        $this->zipComment = $newComment;

        return true;
    }

    /**
     * Set zip file to write zip data to.
     * This will cause all present and future data written to this class to be written to this file.
     * This can be used at any time, even after the Zip Archive have been finalized. Any previous file will be closed.
     * Warning: If the given file already exists, it will be overwritten.
     *
     * @param string $fileName
     * @return bool $success
     */
    public function setZipFile($fileName)
    {
        if (is_file($fileName)) {
            unlink($fileName);
        }

        $fd = fopen($fileName, "x+b");
        if (is_resource($this->zipFile)) {
            rewind($this->zipFile);
            while (! feof($this->zipFile)) {
                fwrite($fd, fread($this->zipFile, $this->streamChunkSize));
            }

            fclose($this->zipFile);
        } else {
            fwrite($fd, $this->zipData);
            $this->zipData = null;
        }
        $this->zipFile = $fd;

        return true;
    }

    /**
     * Add an empty directory entry to the zip archive.
     * Basically this is only used if an empty directory is added.
     *
     * @param string $directoryPath
     *            Directory Path and name to be added to the archive.
     * @param int $timestamp
     *            (Optional) Timestamp for the added directory, if omitted or set to 0, the current time will be used.
     * @param string $fileComment
     *            (Optional) Comment to be added to the archive for this directory. To use fileComment,
     * timestamp must be given.
     * @param int $extFileAttr
     *            (Optional) The external file reference, use generateExtAttr to generate this.
     * @return bool $success
     */
    public function addDirectory(
        $directoryPath,
        $timestamp = 0,
        $fileComment = null,
        $extFileAttr = self::EXT_FILE_ATTR_DIR
    ) {
        if ($this->isFinalized) {
            return false;
        }

        $directoryPath = str_replace("\\", "/", $directoryPath);
        $directoryPath = rtrim($directoryPath, "/");

        if (Tools::strlen($directoryPath, "ASCII") > 0) {
            $this->buildZipEntry(
                $directoryPath . '/',
                $fileComment,
                "\x00\x00",
                "\x00\x00",
                $timestamp,
                "\x00\x00\x00\x00",
                0,
                0,
                $extFileAttr
            );
            return true;
        }
        return false;
    }

    /**
     * Add a file to the archive at the specified location and file name.
     *
     * @param string $data
     *            File data.
     * @param string $filePath
     *            Filepath and name to be used in the archive.
     * @param int $timestamp
     *            (Optional) Timestamp for the added file, if omitted or set to 0, the current time will be used.
     * @param string $fileComment
     *            (Optional) Comment to be added to the archive for this file. To use fileComment,
     * timestamp must be given.
     * @param bool $compress
     *            (Optional) Compress file, if set to false the file will only be stored. Default true.
     * @param int $extFileAttr
     *            (Optional) The external file reference, use generateExtAttr to generate this.
     * @return bool $success
     */
    public function addFile(
        $data,
        $filePath,
        $timestamp = 0,
        $fileComment = null,
        $compress = true,
        $extFileAttr = self::EXT_FILE_ATTR_FILE
    ) {
        if ($this->isFinalized) {
            return false;
        }

        if (is_resource($data) && get_resource_type($data) == "stream") {
            $this->addLargeFile($data, $filePath, $timestamp, $fileComment, $extFileAttr);
            return false;
        }

        $gzData = "";
        $gzType = "\x08\x00";
        $gpFlags = "\x00\x00";
        $dataLength = Tools::strlen($data, "ASCII");
        $fileCRC32 = pack("V", crc32($data));

        if ($compress) {
            $gzTmp = gzcompress($data);
            $gzData = Tools::substr(Tools::substr($gzTmp, 0, Tools::strlen($gzTmp, "ASCII") - 4, 'ASCII'), 2, false, 'ASCII');

            $gzLength = Tools::strlen($gzData, "ASCII");
        } else {
            $gzLength = $dataLength;
        }

        if ($gzLength >= $dataLength) {
            $gzLength = $dataLength;
            $gzData = $data;
            $gzType = "\x00\x00";
            $gpFlags = "\x00\x00";
        }

        if (! is_resource($this->zipFile) && ($this->offset + $gzLength) > $this->zipMemoryThreshold) {
            $this->zipflush();
        }

        $this->buildZipEntry(
            $filePath,
            $fileComment,
            $gpFlags,
            $gzType,
            $timestamp,
            $fileCRC32,
            $gzLength,
            $dataLength,
            $extFileAttr
        );

        $this->zipwrite($gzData);

        return true;
    }

    /**
     * Add the content to a directory.
     *
     * @author Adam Schmalhofer <Adam.Schmalhofer@gmx.de>
     * @author A. Grandt
     *
     * @param string $realPath
     *            Path on the file system.
     * @param string $zipPath
     *            Filepath and name to be used in the archive.
     * @param bool $recursive
     *            Add content recursively, default is true.
     * @param bool $followSymlinks
     *            Follow and add symbolic links, if they are accessible, default is true.
     * @param
     *            array &$addedFiles Reference to the added files, this is used to prevent duplicates,
     * efault is an empty array.
     *            If you start the function by parsing an array, the array will be populated with the realPath
     *            and zipPath kay/value pairs added to the archive by the function.
     * @param bool $overrideFilePermissions
     *            Force the use of the file/dir permissions set in the $extDirAttr
     *            and $extFileAttr parameters.
     * @param int $extDirAttr
     *            Permissions for directories.
     * @param int $extFileAttr
     *            Permissions for files.
     */
    public function addDirectoryContent(
        $realPath,
        $zipPath,
        $recursive = true,
        $followSymlinks = true,
        &$addedFiles = array(),
        $overrideFilePermissions = false,
        $extDirAttr = self::EXT_FILE_ATTR_DIR,
        $extFileAttr = self::EXT_FILE_ATTR_FILE
    ) {
        if (file_exists($realPath) && ! isset($addedFiles[realpath($realPath)])) {
            if (is_dir($realPath)) {
                if ($overrideFilePermissions) {
                    $this->addDirectory($zipPath, 0, null, $extDirAttr);
                } else {
                    $this->addDirectory($zipPath, 0, null, self::getFileExtAttr($realPath));
                }
            }

            $addedFiles[realpath($realPath)] = $zipPath;

            $iter = new DirectoryIterator($realPath);
            foreach ($iter as $file) {
                if ($file->isDot()) {
                    continue;
                }
                $newRealPath = $file->getPathname();
                $newZipPath = self::pathJoin($zipPath, $file->getFilename());

                if (file_exists($newRealPath) && ($followSymlinks === true || ! is_link($newRealPath))) {
                    if ($file->isFile()) {
                        $addedFiles[realpath($newRealPath)] = $newZipPath;
                        if ($overrideFilePermissions) {
                            $this->addLargeFile($newRealPath, $newZipPath, 0, null, $extFileAttr);
                        } else {
                            $this->addLargeFile($newRealPath, $newZipPath, 0, null, self::getFileExtAttr($newRealPath));
                        }
                    } elseif ($recursive === true) {
                        $this->addDirectoryContent(
                            $newRealPath,
                            $newZipPath,
                            $recursive,
                            $followSymlinks,
                            $addedFiles,
                            $overrideFilePermissions,
                            $extDirAttr,
                            $extFileAttr
                        );
                    } else {
                        if ($overrideFilePermissions) {
                            $this->addDirectory($zipPath, 0, null, $extDirAttr);
                        } else {
                            $this->addDirectory($zipPath, 0, null, self::getFileExtAttr($newRealPath));
                        }
                    }
                }
            }
        }
    }

    /**
     * Add a file to the archive at the specified location and file name.
     *
     * @param string $dataFile
     *            File name/path.
     * @param string $filePath
     *            Filepath and name to be used in the archive.
     * @param int $timestamp
     *            (Optional) Timestamp for the added file, if omitted or set to 0, the current time will be used.
     * @param string $fileComment
     *            (Optional) Comment to be added to the archive for this file. To use fileComment,
     * timestamp must be given.
     * @param int $extFileAttr
     *            (Optional) The external file reference, use generateExtAttr to generate this.
     * @return bool $success
     */
    public function addLargeFile(
        $dataFile,
        $filePath,
        $timestamp = 0,
        $fileComment = null,
        $extFileAttr = self::EXT_FILE_ATTR_FILE
    ) {
        if ($this->isFinalized) {
            return false;
        }

        if (is_string($dataFile) && is_file($dataFile)) {
            $this->processFile($dataFile, $filePath, $timestamp, $fileComment, $extFileAttr);
        } else {
            if (is_resource($dataFile) && get_resource_type($dataFile) == "stream") {
                $fh = $dataFile;
                $this->openStream($filePath, $timestamp, $fileComment, $extFileAttr);

                while (! feof($fh)) {
                    $this->addStreamData(fread($fh, $this->streamChunkSize));
                }

                $this->closeStream($this->addExtraField);
            }
        }
        return true;
    }

    /**
     * Create a stream to be used for large entries.
     *
     * @param string $filePath
     *            Filepath and name to be used in the archive.
     * @param int $timestamp
     *            (Optional) Timestamp for the added file, if omitted or set to 0, the current time will be used.
     * @param string $fileComment
     *            (Optional) Comment to be added to the archive for this file. To use fileComment,
     * timestamp must be given.
     * @param int $extFileAttr
     *            (Optional) The external file reference, use generateExtAttr to generate this.
     * @throws Exception Throws an exception in case of errors
     * @return bool $success
     */
    public function openStream($filePath, $timestamp = 0, $fileComment = null, $extFileAttr = self::EXT_FILE_ATTR_FILE)
    {
        if (! function_exists('sys_get_temp_dir')) {
            throw new Exception("Zip " . self::VERSION . " requires PHP version 5.2.1 or above if
            large files are used.");
        }

        if ($this->isFinalized) {
            return false;
        }

        $this->zipflush();

        if (Tools::strlen($this->streamFilePath, "ASCII") > 0) {
            $this->closeStream();
        }

        $this->streamFile = self::getTemporaryFile();
        $this->streamData = fopen($this->streamFile, "wb");
        $this->streamFilePath = $filePath;
        $this->streamTimestamp = $timestamp;
        $this->streamFileComment = $fileComment;
        $this->streamFileLength = 0;
        $this->streamExtFileAttr = $extFileAttr;

        return true;
    }

    /**
     * Add data to the open stream.
     *
     * @param string $data
     * @throws Exception Throws an exception in case of errors
     * @return mixed length in bytes added or false if the archive is finalized or there are no open stream.
     */
    public function addStreamData($data)
    {
        if ($this->isFinalized || Tools::strlen($this->streamFilePath, "ASCII") == 0) {
            return false;
        }

        $length = fwrite($this->streamData, $data, Tools::strlen($data, "ASCII"));
        if ($length != Tools::strlen($data, "ASCII")) {
            throw new Exception(
                "File IO: Error writing; Length mismatch: Expected " . Tools::strlen($data, "ASCII")
                . " bytes, wrote " . ($length === false ? "NONE!" : $length)
            );
        }

        $this->streamFileLength += $length;

        return $length;
    }

    /**
     * Close the current stream.
     *
     * @return bool $success
     */
    public function closeStream()
    {
        if ($this->isFinalized || Tools::strlen($this->streamFilePath, "ASCII") == 0) {
            return false;
        }

        fflush($this->streamData);
        fclose($this->streamData);

        $this->processFile(
            $this->streamFile,
            $this->streamFilePath,
            $this->streamTimestamp,
            $this->streamFileComment,
            $this->streamExtFileAttr
        );

        $this->streamData = null;
        $this->streamFilePath = null;
        $this->streamTimestamp = null;
        $this->streamFileComment = null;
        $this->streamFileLength = 0;
        $this->streamExtFileAttr = null;

        unlink($this->streamFile);

        $this->streamFile = null;

        return true;
    }

    private function processFile(
        $dataFile,
        $filePath,
        $timestamp = 0,
        $fileComment = null,
        $extFileAttr = self::EXT_FILE_ATTR_FILE
    ) {
        if ($this->isFinalized) {
            return false;
        }

        $tempzip = self::getTemporaryFile();

        $zip = new ZipArchive();
        if ($zip->open($tempzip) === true) {
            $zip->addFile($dataFile, 'file');
            $zip->close();
        }

        $file_handle = fopen($tempzip, "rb");
        $stats = fstat($file_handle);
        $eof = $stats['size'] - 72;

        fseek($file_handle, 6);

        $gpFlags = fread($file_handle, 2);
        $gzType = fread($file_handle, 2);
        fread($file_handle, 4);
        $fileCRC32 = fread($file_handle, 4);
        $v = unpack("Vval", fread($file_handle, 4));
        $gzLength = $v['val'];
        $v = unpack("Vval", fread($file_handle, 4));
        $dataLength = $v['val'];

        $this->buildZipEntry(
            $filePath,
            $fileComment,
            $gpFlags,
            $gzType,
            $timestamp,
            $fileCRC32,
            $gzLength,
            $dataLength,
            $extFileAttr
        );

        fseek($file_handle, 34);
        $pos = 34;

        while (! feof($file_handle) && $pos < $eof) {
            $datalen = $this->streamChunkSize;
            if ($pos + $this->streamChunkSize > $eof) {
                $datalen = $eof - $pos;
            }

            $data = fread($file_handle, $datalen);
            $pos += $datalen;

            $this->zipwrite($data);
        }

        fclose($file_handle);

        unlink($tempzip);
    }

    /**
     * Close the archive.
     * A closed archive can no longer have new files added to it.
     *
     * @return bool $success
     */
    public function finalize()
    {
        if (! $this->isFinalized) {
            if (Tools::strlen($this->streamFilePath, "ASCII") > 0) {
                $this->closeStream();
            }

            $cd = implode("", $this->cdRec);

            $cdRecSize = pack("v", sizeof($this->cdRec));
            $cdRec = $cd . self::ZIP_END_OF_CENTRAL_DIRECTORY . $cdRecSize . $cdRecSize .
                pack("VV", Tools::strlen($cd, "ASCII"), $this->offset);
            if (! empty($this->zipComment)) {
                $cdRec .= pack("v", Tools::strlen($this->zipComment, "ASCII")) . $this->zipComment;
            } else {
                $cdRec .= "\x00\x00";
            }

            $this->zipwrite($cdRec);

            $this->isFinalized = true;
            $this->cdRec = null;

            return true;
        }
        return false;
    }

    /**
     * Get the handle ressource for the archive zip file.
     * If the zip haven't been finalized yet, this will cause it to become finalized
     *
     * @return zip file handle
     */
    public function getZipFile()
    {
        if (! $this->isFinalized) {
            $this->finalize();
        }

        $this->zipflush();

        rewind($this->zipFile);

        return $this->zipFile;
    }

    /**
     * Get the zip file contents
     * If the zip haven't been finalized yet, this will cause it to become finalized
     *
     * @return zip data
     */
    public function getZipData()
    {
        if (! $this->isFinalized) {
            $this->finalize();
        }

        if (! is_resource($this->zipFile)) {
            return $this->zipData;
        } else {
            rewind($this->zipFile);
            $filestat = fstat($this->zipFile);
            return fread($this->zipFile, $filestat['size']);
        }
    }

    /**
     * Send the archive as a zip download
     *
     * @param String $fileName
     *            The name of the Zip archive, in ISO-8859-1 (or ASCII) encoding, ie. "archive.zip".
     * Optional, defaults to null, which means that no ISO-8859-1 encoded file name will be specified.
     * @param String $contentType
     *            Content mime type. Optional, defaults to "application/zip".
     * @param String $utf8FileName
     *            The name of the Zip archive, in UTF-8 encoding. Optional, defaults to null,
     * which means that no UTF-8 encoded file name will be specified.
     * @param bool $inline
     *            Use Content-Disposition with "inline" instead of "attached". Optional, defaults to false.
     * @throws Exception Throws an exception in case of errors
     * @return bool Always returns true (for backward compatibility).
     */
    public function sendZip($fileName = null, $contentType = "application/zip", $utf8FileName = null, $inline = false)
    {
        if (! $this->isFinalized) {
            $this->finalize();
        }

        $headerFile = null;
        $headerLine = null;
        if (headers_sent($headerFile, $headerLine)) {
            throw new Exception(
                "Unable to send file '$fileName'. Headers have already
            been sent from '$headerFile' in line $headerLine"
            );
        }

        if (ob_get_contents() !== false && Tools::strlen(ob_get_contents(), "ASCII")) {
            throw new Exception(
                "Unable to send file '$fileName'. Output buffer contains the
            following text (typically warnings or errors):\n" . ob_get_contents()
            );
        }

        if (@ini_get('zlib.output_compression')) {
            @ini_set('zlib.output_compression', 'Off');
        }

        header("Pragma: public");
        header("Last-Modified: " . @gmdate("D, d M Y H:i:s T"));
        header("Expires: 0");
        header("Accept-Ranges: bytes");
        header("Connection: close");
        header("Content-Type: " . $contentType);
        $cd = "Content-Disposition: ";
        if ($inline) {
            $cd .= "inline";
        } else {
            $cd .= "attached";
        }

        if ($fileName) {
            $cd .= '; filename="' . $fileName . '"';
        }

        if ($utf8FileName) {
            $cd .= "; filename*=UTF-8''" . rawurlencode($utf8FileName);
        }

        header($cd);
        header("Content-Length: " . $this->getArchiveSize());
        if (! is_resource($this->zipFile)) {
            echo $this->zipData;
        } else {
            rewind($this->zipFile);
            while (! feof($this->zipFile)) {
                echo fread($this->zipFile, $this->streamChunkSize);
            }
        }
        return true;
    }

    /**
     * Return the current size of the archive
     *
     * @return $size Size of the archive
     */
    public function getArchiveSize()
    {
        if (! is_resource($this->zipFile)) {
            return Tools::strlen($this->zipData, "ASCII");
        }

        $filestat = fstat($this->zipFile);

        return $filestat['size'];
    }

    /**
     * Calculate the 2 byte dostime used in the zip entries.
     *
     * @param int $timestamp
     * @return 2-byte encoded DOS Date
     */
    private function getDosTime($timestamp = 0)
    {
        $timestamp = (int) $timestamp;
        $oldTZ = @date_default_timezone_get();
        date_default_timezone_set('UTC');
        $date = ($timestamp == 0 ? getdate() : getdate($timestamp));
        date_default_timezone_set($oldTZ);
        if ($date["year"] >= 1980) {
            return pack(
                "V",
                (($date["mday"] + ($date["mon"] << 5) + (($date["year"] - 1980) << 9)) << 16)
                | (($date["seconds"] >> 1) + ($date["minutes"] << 5) + ($date["hours"] << 11))
            );
        }

        return "\x00\x00\x00\x00";
    }

    /**
     * Build the Zip file structures
     *
     * @param string $filePath
     * @param string $fileComment
     * @param string $gpFlags
     * @param string $gzType
     * @param int $timestamp
     * @param string $fileCRC32
     * @param int $gzLength
     * @param int $dataLength
     * @param int $extFileAttr
     *            Use self::EXT_FILE_ATTR_FILE for files, self::EXT_FILE_ATTR_DIR for Directories.
     */
    private function buildZipEntry(
        $filePath,
        $fileComment,
        $gpFlags,
        $gzType,
        $timestamp,
        $fileCRC32,
        $gzLength,
        $dataLength,
        $extFileAttr
    ) {
        $filePath = str_replace("\\", "/", $filePath);
        $fileCommentLength = (empty($fileComment) ? 0 : Tools::strlen($fileComment, "ASCII"));
        $timestamp = (int) $timestamp;
        $timestamp = ($timestamp == 0 ? time() : $timestamp);

        $dosTime = $this->getDosTime($timestamp);
        $tsPack = pack("V", $timestamp);

        if (!isset($gpFlags) || Tools::strlen($gpFlags, "ASCII") != 2) {
            $gpFlags = "\x00\x00";
        }

        $isFileUTF8 = mb_check_encoding($filePath, "UTF-8") && ! mb_check_encoding($filePath, "ASCII");
        $isCommentUTF8 = !empty($fileComment) && mb_check_encoding($fileComment, "UTF-8")
            && !mb_check_encoding($fileComment, "ASCII");

        $localExtraField = "";
        $centralExtraField = "";

        if ($this->addExtraField) {
            $localExtraField .= "\x55\x54\x09\x00\x03" . $tsPack . $tsPack . Zip::EXTRA_FIELD_NEW_UNIX_GUID;
            $centralExtraField .= "\x55\x54\x05\x00\x03" . $tsPack . Zip::EXTRA_FIELD_NEW_UNIX_GUID;
        }

        if ($isFileUTF8 || $isCommentUTF8) {
            $flag = 0;
            $gpFlagsV = unpack("vflags", $gpFlags);
            if (isset($gpFlagsV['flags'])) {
                $flag = $gpFlagsV['flags'];
            }

            $gpFlags = pack("v", $flag | (1 << 11));

            if ($isFileUTF8) {
                $utfPathExtraField = "\x75\x70" . pack("v", (5 + Tools::strlen($filePath, "ASCII")))
                    . "\x01" . pack("V", crc32($filePath)) . $filePath;

                $localExtraField .= $utfPathExtraField;
                $centralExtraField .= $utfPathExtraField;
            }
            if ($isCommentUTF8) {
                $centralExtraField .= "\x75\x63" . pack("v", (5 + Tools::strlen($fileComment, "ASCII")))
                    . "\x01" . pack("V", crc32($fileComment)) . $fileComment;
            }
        }

        $header = $gpFlags . $gzType . $dosTime . $fileCRC32
            . pack("VVv", $gzLength, $dataLength, Tools::strlen($filePath, "ASCII"));

        $zipEntry = self::ZIP_LOCAL_FILE_HEADER . self::ATTR_VERSION_TO_EXTRACT . $header
            . pack("v", Tools::strlen($localExtraField, "ASCII")) . $filePath . $localExtraField;

        $this->zipwrite($zipEntry);

        $cdEntry = self::ZIP_CENTRAL_FILE_HEADER . self::ATTR_MADE_BY_VERSION
            . ($dataLength === 0 ? "\x0A\x00" : self::ATTR_VERSION_TO_EXTRACT)
            . $header . pack("v", Tools::strlen($centralExtraField, "ASCII")) . pack("v", $fileCommentLength)
            . "\x00\x00" . "\x00\x00" . pack("V", $extFileAttr) . pack("V", $this->offset)
            . $filePath . $centralExtraField;

        if (! empty($fileComment)) {
            $cdEntry .= $fileComment;
        }

        $this->cdRec[] = $cdEntry;
        $this->offset += Tools::strlen($zipEntry, "ASCII") + $gzLength;
    }

    private function zipwrite($data)
    {
        if (! is_resource($this->zipFile)) {
            $this->zipData .= $data;
        } else {
            fwrite($this->zipFile, $data);
            fflush($this->zipFile);
        }
    }

    private function zipflush()
    {
        if (! is_resource($this->zipFile)) {
            $this->zipFile = tmpfile();
            fwrite($this->zipFile, $this->zipData);
            $this->zipData = null;
        }
    }

    /**
     * Join $file to $dir path, and clean up any excess slashes.
     *
     * @param string $dir
     * @param string $file
     */
    public static function pathJoin($dir, $file)
    {
        if (empty($dir) || empty($file)) {
            return self::getRelativePath($dir . $file);
        }

        return self::getRelativePath($dir . '/' . $file);
    }

    /**
     * Clean up a path, removing any unnecessary elements such as /./,
     * If the path starts with a "/", it is deemed an absolute path and any /../ in the beginning is stripped off.
     * The returned path will not end in a "/".
     *
     * Sometimes, when a path is generated from multiple fragments,
     * you can get something like "../data/html/../images/image.jpeg"
     * This will normalize that example path to "../data/images/image.jpeg"
     *
     * @param string $path
     *            The path to clean up
     * @return string the clean path
     */
    public static function getRelativePath($path)
    {
        $path = preg_replace("#/+\.?/+#", "/", str_replace("\\", "/", $path));
        $dirs = explode("/", rtrim(preg_replace('#^(?:\./)+#', '', $path), '/'));

        $offset = 0;
        $sub = 0;
        $subOffset = 0;
        $root = "";

        if (empty($dirs[0])) {
            $root = "/";
            $dirs = array_splice($dirs, 1);
        } else {
            if (preg_match("#[A-Za-z]:#", $dirs[0])) {
                $root = Tools::strtoupper($dirs[0]) . "/";
                $dirs = array_splice($dirs, 1);
            }
        }

        $newDirs = array();
        foreach ($dirs as $dir) {
            if ($dir !== "..") {
                $subOffset --;
                $newDirs[++ $offset] = $dir;
            } else {
                $subOffset ++;
                if (-- $offset < 0) {
                    $offset = 0;
                    if ($subOffset > $sub) {
                        $sub ++;
                    }
                }
            }
        }

        if (empty($root)) {
            $root = str_repeat("../", $sub);
        }

        return $root . implode("/", array_slice($newDirs, 0, $offset));
    }

    /**
     * Create the file permissions for a file or directory, for use in the extFileAttr parameters.
     *
     * @param int $owner
     *            Unix permisions for owner (octal from 00 to 07)
     * @param int $group
     *            Unix permisions for group (octal from 00 to 07)
     * @param int $other
     *            Unix permisions for others (octal from 00 to 07)
     * @param bool $isFile
     * @return EXTRERNAL_REF field.
     */
    public static function generateExtAttr($owner = 07, $group = 05, $other = 05, $isFile = true)
    {
        $fp = $isFile ? self::S_IFREG : self::S_IFDIR;
        $fp |= (($owner & 07) << 6) | (($group & 07) << 3) | ($other & 07);

        return ($fp << 16) | ($isFile ? self::S_DOS_A : self::S_DOS_D);
    }

    /**
     * Get the file permissions for a file or directory, for use in the extFileAttr parameters.
     *
     * @param string $filename
     * @return external ref field, or false if the file is not found.
     */
    public static function getFileExtAttr($filename)
    {
        if (file_exists($filename)) {
            $fp = fileperms($filename) << 16;
            return $fp | (is_dir($filename) ? self::S_DOS_D : self::S_DOS_A);
        }
        return false;
    }

    /**
     * Returns the path to a temporary file.
     *
     * @return string
     */
    private static function getTemporaryFile()
    {
        if (is_callable(self::$temp)) {
            $temporaryFile = @call_user_func(self::$temp);
            if (is_string($temporaryFile) && Tools::strlen($temporaryFile, "ASCII") && is_writable($temporaryFile)) {
                return $temporaryFile;
            }
        }
        $temporaryDirectory = (is_string(self::$temp) && Tools::strlen(self::$temp, "ASCII")) ? self::$temp : sys_get_temp_dir();
        return tempnam($temporaryDirectory, 'Zip');
    }
}
