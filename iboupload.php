<?php
/**
* 2015 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-Design Dominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt. 
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2015 Dominik Labudzinski
*  @license   Commercial
*/

if (! defined('_PS_VERSION_')) {
    exit();
}
require_once('library/install.php');

class IboUpload extends Module
{
    public function __construct()
    {
        $this->bootstrap = true;
        $this->name = 'iboupload';
        $this->tab = 'front_office_features';
        $this->version = '2.1.3';
        $this->author = 'labudzinski.com';
        $this->module_key = '3a75e487a4fed932510acfc5db2fad58';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array(
            'min' => '1.5.3.1',
            'max' => _PS_VERSION_
        );
        parent::__construct();
        $this->displayName = $this->l('Customer files uploader');
        $this->description = $this->l('Customer files uploader');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        //$this->registerHook('backOfficeHeader');
        //$this->registerHook('displayBackOfficeHeader');
    }

    /**
     * @return mixed
     */
    public function hookDisplayAdminOrderTabOrder()
    {
        return $this->display(__FILE__, 'views/templates/admin/orderTab.tpl');
    }

    public function hookBackOfficeHeader($params)
    {
        $this->context->controller->addCss($this->getPathUri().'views/css/admin.css', 'all');
    }

    public function hookDisplayBackOfficeHeader($params)
    {
        $this->hookBackOfficeHeader($params);
    }

    public function hookDisplayAdminOrderContentOrder()
    {
        $files = $this->getFilesForOrder();
        $this->context->smarty->assign(array(
            'uproducts' => $files
        ));
        return $this->hookDisplayAdminOrder();
    }

    /**
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayAdminOrder()
    {
        $files = $this->getFilesForOrder();
        $this->context->smarty->assign(array(
            'uproducts' => $files,
            'id_order' => (int)Tools::getValue('id_order')
        ));
        if (version_compare(_PS_VERSION_, '1.6', '>=')) {
            return $this->display(__FILE__, 'views/templates/admin/order.tpl');
        } else {
            return $this->display(__FILE__, 'views/templates/admin/order15.tpl');
        }
    }

    /**
     * @param $params
     */
    public function hookDisplayCustomerAccount($params)
    {
        //return $this->display(__FILE__, 'views/templates/front/account-link.tpl');
    }

    /**
     * @param $params
     * @throws PrestaShopDatabaseException
     */
    public function hookActionValidateOrder($params)
    {
        $order = new Order($params['order']->id);
        $details = $order->getOrderDetailList();
        foreach ($params['order']->product_list as $product) {
            $result = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_upload 
	   WHERE id_cart = '.(int)$params['cart']->id.'
	   AND id_product = '.$product['id_product']);
            foreach ($details as $detail) {
                if ($detail['id_order'] == $params['order']->id && $detail['product_id'] == $product['id_product']) {
                    foreach ($result as $res) {
                        Db::getInstance()->insert('order_upload', array(
                            'id_order_detail' => (int)$detail['id_order_detail'],
                            'id_product' => (int)$detail['product_id'],
                            'id_product_attribute' => (int)$detail['product_attribute_id'],
                            'id_order_detail' => (int)$detail['id_order_detail'],
                            'file_path'   => pSQL($res['file_path']),
                            'file_hash' => pSQL(md5($res['file_hash'])),
                            'file_name' => pSql(basename($res['file_name']))
                        ));
                        Db::getInstance()->delete('cart_upload', 'id = '.$res['id']);
                    }
                }
            }
        }
    }

    /**
     * @param $params
     * @return mixed
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayOrderDetail($params)
    {
        $files = array();
        $details = $params['order']->getOrderDetailList();
        foreach ($details as $detail) {
            $result = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'order_upload
WHERE id_order_detail = '.$detail['id_order_detail']);
            foreach ($result as $item) {
                $product = new Product($item['id_product']);
                if ($product->id > 0) {
                    $product_attribute_string = array();
                    $url = str_replace(_PS_UPLOAD_DIR_, '', $item['file_path']);
                    
                    $attributtes = Product::getAttributesParams($item['id_product'], $item['id_product_attribute']);
                    $product_attribute_string[] = $product->name[$this->context->language->id];
        
                    foreach ($attributtes as $attributte) {
                        $product_attribute_string[] = $attributte['group'].': '.$attributte['name'];
                    }
                    if (!isset($files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']])) {
                        if (Tools::file_exists_no_cache(_PS_UPLOAD_DIR_.$url)) {
                            $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['name'] = $item['file_name'];
                            $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['size'] = filesize($item['file_path']);
                            $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['url'] = '/upload/'.$url;
                        }
                    }
                }
            }
        }
        $this->context->smarty->assign(array(
            'uproducts' => $files,
        ));
        return $this->display(__FILE__, 'views/templates/front/cart.tpl');
    }

    /**
     * @param $params
     * @return mixed
     * @throws PrestaShopDatabaseException
     */
    public function hookDisplayShoppingCart($params)
    {
        $files = array();
        $result = Db::getInstance()->executeS('SELECT * FROM '._DB_PREFIX_.'cart_upload WHERE id_cart = '.(int)$params['cart']->id);
        foreach ($result as $item) {
            $product = new Product($item['id_product']);
            if ($product->id > 0) {
                $product_attribute_string = array();
                $url = str_replace(_PS_UPLOAD_DIR_, '', $item['file_path']);
                
                $attributtes = Product::getAttributesParams($item['id_product'], $item['id_product_attribute']);
                $product_attribute_string[] = $product->name[$this->context->language->id];
    
                foreach ($attributtes as $attributte) {
                    $product_attribute_string[] = $attributte['group'].': '.$attributte['name'];
                }
                if (!isset($files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']])) {
                    if (Tools::file_exists_no_cache(_PS_UPLOAD_DIR_.$url)) {
                        $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['name'] = $item['file_name'];
                        $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['size'] = filesize($item['file_path']);
                        $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['url'] = '/upload/'.$url;
                    }
                }
            }
        }
        $this->context->smarty->assign(array(
            'uproducts' => $files,
        ));

        return $this->display(__FILE__, 'views/templates/front/cart.tpl');
    }

    /**
     * @param $params
     * @throws PrestaShopDatabaseException
     */
    public function hookActionCartSave($params)
    {
        if ((Tools::getIsset('delete')) && (Tools::getValue('delete') == 'true')) {
            Db::getInstance()->delete('cart_upload', 'id_product = '.(int)Tools::getValue('id_product'));
            return;
        } elseif (Tools::getIsset('ajax') && $_REQUEST['ajax'] == 'true') {
            $files = $this->context->cookie->getFamily('file_'.(int)Tools::getValue('id_product').'_');
            foreach ($files as $key => $file) {
                Db::getInstance()->insert('cart_upload', array(
                    'id_product' => (int)Tools::getValue('id_product'),
                    'id_product_attribute' => (int)Tools::getValue('ipa'),
                    'id_cart'    => (int)$params['cart']->id,
                    'file_path'   => pSQL($file),
                    'file_hash' => pSQL(md5($file)),
                    'file_name' => pSql(basename($file))
                ));
                $this->context->cookie->__unset($key);
            }
        }
    }

    /**
     *
     */
    public function hookDisplayProductButtons($params)
    {
        return $this->hookDisplayRightColumnProduct($params);
    }

    /**
     * @param $params
     * @return bool|void
     */
    public function hookDisplayRightColumnProduct($params)
    {
        if (Configuration::get('PS_CATALOG_MODE')) {
            return;
        }


        $can_upload = (bool)Db::getInstance()->getValue('SELECT can_upload FROM '._DB_PREFIX_.'product
			WHERE id_product = '.(int)Tools::getValue('id_product'));
        $only_for_logged = (bool)Configuration::get('ibo_only_logged');

        if ($only_for_logged && !$this->context->customer->isLogged()) {
            return false;
        }
        if ($can_upload === true) {
            if ((int)Configuration::get('PS_BLOCK_CART_AJAX')) {
                $this->context->controller->addJs($this->_path.'views/js/plupload.full.min.js');
                $this->context->controller->addCss($this->_path.'views/css/iboupload.css');
                $this->context->controller->addJs($this->_path.'views/js/i18n/'.$this->context->language->iso_code.'.js');
            }

            Tools::getValue('id_product');
            $allow = array();
            if (Configuration::get('ibo_allow_pdf')) {
                $allow[] = array('title' => 'Document files', 'extensions' => 'pdf');
            }

            if (Configuration::get('ibo_allow_zip')) {
                $allow[] = array('title' => 'Zip files', 'extensions' => 'zip');
            }

            if (Configuration::get('ibo_allow_image')) {
                $allow[] = array('title' => 'Image files', 'extensions' => 'jpg,gif,png');
            }

            if (Configuration::get('ibo_allow_ai')) {
                $allow[] = array('title' => 'Adobe files', 'extensions' => 'ai,ait,eps,psd');
            }

            if (Configuration::get('ibo_allow_custom')) {
                $customFiles = Configuration::get('ibo_allow_custom');
                $customFiles = str_replace(" ", "", $customFiles);
                $allow[] = array('title' => 'Custom files', 'extensions' => $customFiles);
            }

            $files = array();
            $tmp = $this->context->cookie->getFamily('file_'.(int)Tools::getValue('id_product').'_');
            foreach ($tmp as $tmp_file) {
                if (file_exists($tmp_file)) {
                    $ext = pathinfo($tmp_file, PATHINFO_EXTENSION);
                    $files[] = array('id' => basename($tmp_file, '.'.$ext), 'name' => basename($tmp_file), 'size' => filesize($tmp_file));
                }
            }
            $this->context->smarty->assign(array(
                'ibo_max_file_size' => Configuration::get('ibo_max_file_size'),
                'ibo_allow' => Tools::jsonEncode($allow),
                'id_product' => (int)Tools::getValue('id_product'),
                'multi_selection' => (bool)Db::getInstance()->getValue('SELECT multiupload FROM '._DB_PREFIX_.'product
										WHERE id_product = '.(int)Tools::getValue('id_product')),
                'maximum_files' => Db::getInstance()->getValue('SELECT maximum_files FROM '. _DB_PREFIX_.'product
                                        WHERE id_product = '.(int)Tools::getValue('id_product')),
                'unique_names' => (bool)Configuration::get('ibo_unique_names'),
                'files' => $files,
            ));
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                return $this->display(__FILE__, 'views/templates/front/product.tpl');
            } else {
                return $this->display(__FILE__, 'views/templates/front/product15.tpl');
            }
        }
    }

    /**
     * @return mixed
     */
    public function hookDisplayAdminProductsExtra()
    {
        /** @var ProductCore $product */
        $product = new Product((int)Tools::getValue('id_product'));
        if (Validate::isLoadedObject($product)) {
            $combinations = $this->getCombinations($product);
            $this->context->smarty->assign(array(
                'can_upload' => Db::getInstance()->getValue(
                    'SELECT can_upload FROM '.
                    _DB_PREFIX_.'product WHERE id_product = '.(int)Tools::getValue('id_product')
                ),
                'multiupload' => Db::getInstance()->getValue(
                    'SELECT multiupload FROM '.
                    _DB_PREFIX_.'product WHERE id_product = '.(int)Tools::getValue('id_product')
                ),
                'maximum_files' => Db::getInstance()->getValue(
                    'SELECT maximum_files FROM '.
                    _DB_PREFIX_.'product WHERE id_product = '.(int)Tools::getValue('id_product')
                ),
                'combinations' => $combinations
            ));
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                return $this->display(__FILE__, 'views/templates/admin/product.tpl');
            } else {
                return $this->display(__FILE__, 'views/templates/admin/product15.tpl');
            }
        }
    }

    /**
     * @param $params
     */
    public function hookActionProductSave($params)
    {
        $canUpload = Tools::getValue('can_upload');
        $multiUpload = Tools::getValue('multiupload');
        $maximumFiles = Tools::getValue('maximum_files');

        if (is_array($canUpload)) {
            foreach ($canUpload as $id => $value) {
                if (!Db::getInstance()->update('product_attribute', array(
                    'can_upload' => pSQL((int)$value)
                ), 'id_product_attribute = ' . (int)$id)
                ) {
                    $this->context->controller->_errors[] = Tools::displayError('Error: ') . mysql_error();
                }
            }
        } else {
            if (!Db::getInstance()->update('product', array(
                'can_upload' => pSQL($canUpload)
            ), 'id_product = ' . (int)$params['id_product'])
            ) {
                $this->context->controller->_errors[] = Tools::displayError('Error: ') . mysql_error();
            }
        }
        if (is_array($maximumFiles)) {
            foreach ($maximumFiles as $id => $value) {
                if (!Db::getInstance()->update('product_attribute', array(
                    'maximum_files' => (int)$value
                ), 'id_product_attribute = ' . (int)$id)
                ) {
                    $this->context->controller->_errors[] = Tools::displayError('Error: ') . mysql_error();
                }
            }
        } else {
            if (!Db::getInstance()->update('product', array(
                'maximum_files' => pSQL((int)$maximumFiles)
            ), 'id_product = ' . (int)$params['id_product'])
            ) {
                $this->context->controller->_errors[] = Tools::displayError('Error: ') . mysql_error();
            }
        }
        if (is_array($multiUpload)) {
            foreach ($multiUpload as $id => $value) {
                if (!Db::getInstance()->update('product_attribute', array(
                    'multiupload' => pSQL((int)$value)
                ), 'id_product_attribute = ' . (int)$id)
                ) {
                    $this->context->controller->_errors[] = Tools::displayError('Error: ') . mysql_error();
                }
            }
        } else {
            if (!Db::getInstance()->update('product', array(
                'multiupload' => pSQL($multiUpload)
            ), 'id_product = ' . (int)$params['id_product'])
            ) {
                $this->context->controller->_errors[] = Tools::displayError('Error: ') . mysql_error();
            }
        }
    }

    /**
     *
     * @return string
     */
    public function getContent()
    {
        $output = '';
        if (Tools::isSubmit('submitIboUpload')) {
            if (Configuration::get('ibo_only_logged') != Tools::getValue('ibo_only_logged')) {
                Configuration::updateValue('ibo_only_logged', Tools::getValue('ibo_only_logged'));
            }

            if (Configuration::get('ibo_aviary') != Tools::getValue('ibo_aviary')) {
                Configuration::updateValue('ibo_aviary', Tools::getValue('ibo_aviary'));
            }

            if (Configuration::get('ibo_aviary_apiKey') != Tools::getValue('ibo_aviary_apiKey')) {
                Configuration::updateValue('ibo_aviary_apiKey', Tools::getValue('ibo_aviary_apiKey'));
            }

            if (Configuration::get('ibo_show_in_cart') != Tools::getValue('ibo_show_in_cart')) {
                Configuration::updateValue('ibo_show_in_cart', Tools::getValue('ibo_show_in_cart'));
            }

            if (Configuration::get('ibo_show_in_history') != Tools::getValue('ibo_show_in_history')) {
                Configuration::updateValue('ibo_show_in_history', Tools::getValue('ibo_show_in_history'));
            }

            if (Configuration::get('ibo_show_on_invoice') != Tools::getValue('ibo_show_on_invoice')) {
                Configuration::updateValue('ibo_show_on_invoice', Tools::getValue('ibo_show_on_invoice'));
            }

            if (Configuration::get('ibo_allow_zip') != Tools::getValue('ibo_allow_zip')) {
                Configuration::updateValue('ibo_allow_zip', Tools::getValue('ibo_allow_zip'));
            }

            if (Configuration::get('ibo_allow_pdf') != Tools::getValue('ibo_allow_pdf')) {
                Configuration::updateValue('ibo_allow_pdf', Tools::getValue('ibo_allow_pdf'));
            }

            if (Configuration::get('ibo_allow_ai') != Tools::getValue('ibo_allow_ai')) {
                Configuration::updateValue('ibo_allow_ai', Tools::getValue('ibo_allow_ai'));
            }

            if (Configuration::get('ibo_allow_custom') != Tools::getValue('ibo_allow_custom')) {
                Configuration::updateValue('ibo_allow_custom', Tools::getValue('ibo_allow_custom'));
            }

            if (Configuration::get('ibo_allow_image') != Tools::getValue('ibo_allow_image')) {
                Configuration::updateValue('ibo_allow_image', Tools::getValue('ibo_allow_image'));
            }

            if (Configuration::get('ibo_upload_script') != Tools::getValue('ibo_upload_script')) {
                Configuration::updateValue('ibo_upload_script', Tools::getValue('ibo_upload_script'));
            }

            if (Configuration::get('ibo_unique_names') != Tools::getValue('ibo_unique_names')) {
                Configuration::updateValue('ibo_unique_names', Tools::getValue('ibo_unique_names'));
            }

            $ibo_max_file_size = (Tools::getValue('ibo_max_file_size') > 0)?Tools::getValue('ibo_max_file_size'):1024;

            if (Configuration::get('ibo_max_file_size') != $ibo_max_file_size) {
                Configuration::updateValue('ibo_max_file_size', $ibo_max_file_size);
            }

            $output = $this->createUploadDirectory();

            if (empty($output)) {
                $output = $this->displayInformation($this->l('Nothing has been changed.'));
            }
        }
        //$this->display(__FILE__, 'views/templates/admin/module.tpl')
        return $output.$this->renderForm();
    }

    /**
     * @return string|null
     */
    private function createUploadDirectory()
    {
        if (!is_writable(Configuration::get('ibo_upload_dir'))) {
            if (@mkdir(Configuration::get('ibo_upload_dir'), 0777, true) === false) {
                return $this->displayError($this->l(sprintf('Can`t create upload directory: %s', Configuration::get('ibo_upload_dir'))));
            }
        }

        if (Configuration::get('ibo_upload_dir') != Tools::getValue('ibo_upload_dir')) {
            Configuration::updateValue('ibo_upload_dir', Tools::getValue('ibo_upload_dir'));
        }

        return null;
    }

    /**
     * @param $string
     * @return string
     */
    public function displayInformation($string)
    {
        $output = '
	 	<div class="bootstrap">
		<div class="module_confirmation conf confirm alert alert-info">
			<button type="button" class="close" data-dismiss="alert">&times;</button>
			'.$string.'
		</div>
		</div>';
        return $output;
    }

    /**
     *
     * @return mixed
     */
    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->l('Settings'),
                    'icon' => 'icon-cogs'
                ),
                'input' => array(),
                'submit' => array(
                    'title' => $this->l('Save'),
                    'class' => 'btn btn-default'
                )
            )
        );

        $fields_form['form']['input'] = array(
            array(
                'type' => 'text',
                'label' => $this->l('Max file size'),
                'name' => 'ibo_max_file_size',
                'class' => 'fixed-width',
                'desc' => $this->l('Set up the max file size (in kilobytes). example: 1024 = 1mb'),
                'required' => true,
                'size' => 96,
            ),
            array(
                'type' => Tools::version_compare(_PS_VERSION_, '1.5.9.9', '>') ? 'switch' : 'radio',
                'label' => $this->l('Allow images'),
                'name' => 'ibo_allow_image',
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                    )
                ),
                'is_bool' => true,
                'class' => 'radioCheck',
            ),
            array(
                'type' => Tools::version_compare(_PS_VERSION_, '1.5.9.9', '>') ? 'switch' : 'radio',
                'label' => $this->l('Allow ZIP'),
                'name' => 'ibo_allow_zip',
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                    )
                ),
                'is_bool' => true,
                'class' => 'radioCheck',
            ),
            array(
                'type' => Tools::version_compare(_PS_VERSION_, '1.5.9.9', '>') ? 'switch' : 'radio',
                'label' => $this->l('Allow PDF'),
                'name' => 'ibo_allow_pdf',
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                    )
                ),
                'is_bool' => true,
                'class' => 'radioCheck',
            ),
            array(
                'type' => Tools::version_compare(_PS_VERSION_, '1.5.9.9', '>') ? 'switch' : 'radio',
                'label' => $this->l('Allow Adobe files'),
                'desc' => $this->l('eg. ai, ait, eps, psd'),
                'name' => 'ibo_allow_ai',
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                    )
                ),
                'is_bool' => true,
                'class' => 'radioCheck',
            ),
            array(
                'type' => Tools::version_compare(_PS_VERSION_, '1.5.9.9', '>') ? 'switch' : 'radio',
                'label' => $this->l('Unique names'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                    )
                ),
                'is_bool' => true,
                'name' => 'ibo_unique_names',
                'class' => 'radioCheck',
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Custom file extensions'),
                'desc' => $this->l('File extensions separated by a comma without dots and spaces. eg. "png, zip, pdf"'),
                'class' => 'fixed-width',
                'name' => 'ibo_allow_custom',
                'required' => false,
                'disabled' => false,
                'size' => 96,
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Upload directory'),
                'class' => 'fixed-width',
                'name' => 'ibo_upload_dir',
                'required' => true,
                'disabled' => false,
                'size' => 96,
            ),
            array(
                'type' => Tools::version_compare(_PS_VERSION_, '1.5.9.9', '>') ? 'switch' : 'radio',
                'label' => $this->l('Only for logged clients'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                    )
                ),
                'is_bool' => true,
                'name' => 'ibo_only_logged',
                'class' => 'radioCheck',
            ),
            /*array(
                'type' => Tools::version_compare(_PS_VERSION_, '1.5.9.9', '>') ? 'switch' : 'radio',
                'label' => $this->l('Aviary (New)'),
                'values' => array(
                    array(
                        'id' => 'active_on',
                        'value' => 1,
                        'label' => $this->l('Enabled')
                    ),
                    array(
                        'id' => 'active_off',
                        'value' => 0,
                        'label' => $this->l('Disabled')
                    )
                ),
                'is_bool' => true,
                'name' => 'ibo_aviary',
                'class' => 'radioCheck',
            ),*/
        );
        $configFieldsValues = $this->getConfigFieldsValues();
        /*if (isset($configFieldsValues['ibo_aviary']) && $configFieldsValues['ibo_aviary'] == 1) {
            $fields_form['form']['input'][] = array(
                'type' => 'text',
                'label' => $this->l('Aviary apiKey'),
                'name' => 'ibo_aviary_apiKey',
                'class' => 'fixed-width',
                'required' => true,
                'size' => 96,
            );
        }*/

        $helper = new HelperForm();
        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true; // false -> remove toolbar
        $helper->toolbar_scroll = true; // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submitIboUpload';

        $helper->tpl_vars = array(
            'fields_value' => $configFieldsValues
        );
        return $helper->generateForm(array(
            $fields_form
        ));
    }

    /**
     * @return array
     */
    public function getConfigFieldsValues()
    {
        return array(
            'ibo_upload_dir' => Configuration::get('ibo_upload_dir'),
            'ibo_upload_script' => Tools::getValue('ibo_upload_script', Configuration::get('ibo_upload_script')),
            'ibo_allow_image' => Tools::getValue('ibo_allow_image', Configuration::get('ibo_allow_image')),
            'ibo_allow_zip' => Tools::getValue('ibo_allow_zip', Configuration::get('ibo_allow_zip')),
            'ibo_allow_pdf' => Tools::getValue('ibo_allow_pdf', Configuration::get('ibo_allow_pdf')),
            'ibo_allow_ai' => Tools::getValue('ibo_allow_ai', Configuration::get('ibo_allow_ai')),
            'ibo_allow_custom' => Tools::getValue('ibo_allow_custom', Configuration::get('ibo_allow_custom')),
            'ibo_max_file_size' => Tools::getValue('ibo_max_file_size', Configuration::get('ibo_max_file_size')),
            'ibo_only_logged' => Tools::getValue('ibo_only_logged', Configuration::get('ibo_only_logged')),
            'ibo_show_in_history' => Tools::getValue('ibo_show_in_history', Configuration::get('ibo_show_in_history')),
            'ibo_show_in_cart' => Tools::getValue('ibo_show_in_cart', Configuration::get('ibo_show_in_cart')),
            'ibo_show_on_invoice' => Tools::getValue('ibo_show_on_invoice', Configuration::get('ibo_show_on_invoice')),
            'ibo_unique_names' => Tools::getValue('ibo_unique_names', Configuration::get('ibo_unique_names')),
            'ibo_aviary' => Tools::getValue('ibo_aviary', Configuration::get('ibo_aviary')),
            'ibo_aviary_apiKey' => Tools::getValue('ibo_aviary_apiKey', Configuration::get('ibo_aviary_apiKey')),
        );
    }

    /**
     * @return string
     */
    public function hookAjaxCall()
    {
        if ($_REQUEST['action'] == 'remove') {
            if ($_REQUEST['file']) {
                $tmp = $this->context->cookie->getFamily('file_'.(int)Tools::getValue('id_product').'_');
                try {
                    $files = glob(Configuration::get('ibo_upload_dir').DIRECTORY_SEPARATOR.$_REQUEST['file'].'.*');
                    foreach ($files as $file) {
                        if (in_array($file, $tmp)) {
                            unlink($file);
                        }
                    }
                    $this->context->cookie->__unset('file_'.$_REQUEST['id_product'].'_'.$_REQUEST['file']);
                } catch (Exception $e) {
                    return Tools::jsonEncode(array($e->getMessage()));
                }
                return Tools::jsonEncode(array('removed'));
            }
            return Tools::jsonEncode(array('error'));
        }
        $cleanup_target_dir = true;
        $max_file_age = 5 * 3600;
        if (Tools::getIsset('name')) {
            $file_name = $_REQUEST['name'];
        } elseif (! empty($_FILES)) {
            $file_name = $_FILES['file']['name'];
        } else {
            $file_name = uniqid('file_');
        }

        $file_path = Configuration::get('ibo_upload_dir').DIRECTORY_SEPARATOR.$file_name;
        $chunk = Tools::getIsset('chunk') ? (int)$_REQUEST['chunk'] : 0;
        $chunks = Tools::getIsset('chunks') ? (int)$_REQUEST['chunks'] : 0;
        if ($cleanup_target_dir) {
            if (! is_dir(Configuration::get('ibo_upload_dir')) || ! $dir = opendir(Configuration::get('ibo_upload_dir'))) {
                return ('{"jsonrpc" : "2.0", "error" : {"code": 100, "message": "Failed to open temp directory."}, "id" : "id"}');
            }

            while (($file = readdir($dir)) !== false) {
                $tmpfile_path = Configuration::get('ibo_upload_dir').DIRECTORY_SEPARATOR.$file;

                if ($tmpfile_path == "{$file_path}.part") {
                    continue;
                }

                if (preg_match('/\.part$/', $file) && (filemtime($tmpfile_path) < time() - $max_file_age)) {
                    unlink($tmpfile_path);
                }
            }
            closedir($dir);
        }
        $out = fopen($file_path.'.part', $chunks ? 'ab' : 'wb');
        if (!$out) {
            return ('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
        }

        if (! empty($_FILES)) {
            if ($_FILES['file']['error'] || ! is_uploaded_file($_FILES['file']['tmp_name'])) {
                return ('{"jsonrpc" : "2.0", "error" : {"code": 103, "message": "Failed to move uploaded file."}, "id" : "id"}');
            }

            if (! $in = fopen($_FILES['file']['tmp_name'], 'rb')) {
                return ('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        } else {
            if (! $in = fopen('php://input', 'rb')) {
                return ('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
        }

        while ($buff = fread($in, 4096)) {
            fwrite($out, $buff);
        }

        fclose($out);
        fclose($in);

        if (! $chunks || $chunk == $chunks - 1) {
            rename("{$file_path}.part", $file_path);
            $this->context->cookie->{'file_'.(int)$_REQUEST['id_product'].'_'.md5($file_path)} = $file_path;
        }

        return ('{"jsonrpc" : "2.0", "result" : null, "id" : '.$file_name.'}');
    }

    /**
     * @return bool
     */
    public function install()
    {
        if (Install::installDB()) {
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                PrestaShopLogger::addLog($this->l('IBO Upload: Database Installed.'));
            }
        } else {
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                PrestaShopLogger::addLog($this->l('IBO Upload: Database not installed.'));
            }
            return false;
        }
        if (!parent::install()) {
            return false;
        }
        if (!Install::installMenu()) {
            return false;
        }
        if (!Configuration::updateValue('ibo_upload_dir', _PS_UPLOAD_DIR_.'iboupload')) {
            return false;
        }
        if (!Configuration::updateValue('ibo_max_file_size', 1024)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_allow_image', true)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_upload_script', true)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_allow_zip', false)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_allow_pdf', false)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_allow_ai', false)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_allow_custom', false)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_unique_names', true)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_only_logged', true)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_show_in_history', false)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_show_in_cart', false)) {
            return false;
        }
        if (!Configuration::updateValue('ibo_show_on_invoice', false)) {
            return false;
        }
        if (!$this->registerHook('header')) {
            return false;
        }
        if (!$this->registerHook('backOfficeHeader')) {
            return false;
        }
        if (!$this->registerHook('displayBackOfficeHeader')) {
            return false;
        }
        if (!$this->registerHook('actionProductSave')) {
            return false;
        }
        if (!$this->registerHook('displayRightColumnProduct')) {
            return false;
        }
        if (!$this->registerHook('actionCartSave')) {
            return false;
        }
        if (!$this->registerHook('displayShoppingCart')) {
            return false;
        }
        if (!$this->registerHook('actionObjectDeleteAfter')) {
            return false;
        }
        if (!$this->registerHook('actionValidateOrder')) {
            return false;
        }
        if (!$this->registerHook('displayOrderDetail')) {
            return false;
        }
        if (!$this->registerHook('displayCustomerAccount')) {
            return false;
        }
        if (!$this->registerHook('displayAdminProductsExtra')) {
            return false;
        }
        if (!$this->registerHook('displayAdminOrder')) {
            return false;
        }
        if (!$this->registerHook('DisplayOverrideTemplate')) {
            return false;
        }
        if (!$this->registerHook('displayAdminOrderTabOrder')) {
            return false;
        }
        if (!$this->registerHook('displayAdminOrderContentOrder')) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function uninstall()
    {
        if (Install::uninstallDB()) {
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                PrestaShopLogger::addLog($this->l('IBO Upload: Database Unnstalled.'));
            }
        } else {
            if (version_compare(_PS_VERSION_, '1.6', '>=')) {
                PrestaShopLogger::addLog($this->l('IBO Upload: Database not Uninstalled.'));
            }
            return false;
        }

        if (!Install::uninstallMenu()) {
            return false;
        }
        if (!parent::uninstall()) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_upload_dir')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_allow_image')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_upload_script')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_allow_zip')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_allow_pdf')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_allow_ai')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_allow_custom')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_max_file_size')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_only_logged')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_show_in_history')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_show_in_cart')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_show_on_invoice')) {
            return false;
        }
        if (!Configuration::deleteByName('ibo_unique_names')) {
            return false;
        }

        return true;
    }



    /**
     * @param $product
     * @return array
     */
    private function getCombinations($product)
    {
        $comb_array = array();
        return $comb_array;
        $attributes = $product->getAttributesGroups((int)$this->context->language->id);
        $combinations = array();
        foreach ($attributes as $attribute) {
            $combinations[$attribute['id_product_attribute']][$attribute['group_name']] =
                $attribute['attribute_name'];
        }
        if ($product->id) {
            /* Build attributes combinations */
            $combinations = $product->getAttributeCombinations($this->context->language->id);
            $groups = array();
            if (is_array($combinations)) {
                foreach ($combinations as $k => $combination) {

                    $comb_array[$combination['id_product_attribute']]['id_product_attribute'] = $combination['id_product_attribute'];
                    $comb_array[$combination['id_product_attribute']]['attributes'][] = array(
                        $combination['group_name'],
                        $combination['attribute_name'],
                        $combination['id_attribute']
                    );
                    $comb_array[$combination['id_product_attribute']]['reference'] = $combination['reference'];
                    $comb_array[$combination['id_product_attribute']]['ean13'] = $combination['ean13'];
                    $comb_array[$combination['id_product_attribute']]['upc'] = $combination['upc'];
                    $comb_array[$combination['id_product_attribute']]['can_upload'] = (bool)$combination['can_upload'];
                    $comb_array[$combination['id_product_attribute']]['multiupload'] = (bool)$combination['multiupload'];
                    $comb_array[$combination['id_product_attribute']]['maximum_files'] = $combination['maximum_files'];
                    if ($combination['is_color_group']) {
                        $groups[$combination['id_attribute_group']] = $combination['group_name'];
                    }
                }
            }

            if (isset($comb_array)) {
                foreach ($comb_array as $id_product_attribute => $product_attribute) {
                    $list = '';

                    /* In order to keep the same attributes order */
                    asort($product_attribute['attributes']);

                    foreach ($product_attribute['attributes'] as $attribute) {
                        $list .= $attribute[0] . ' - ' . $attribute[1] . ', ';
                    }

                    $list = rtrim($list, ', ');
                    $comb_array[$id_product_attribute]['attributes'] = $list;
                    $comb_array[$id_product_attribute]['name'] = $list;

                }
            }
        }
        return $comb_array;
    }

    /**
     * @return array
     */
    private function getFilesForOrder()
    {
        $files = array();
        $downloads = array();
        $order = new Order((int)Tools::getValue('id_order'));
        $details = $order->getOrderDetailList();
        foreach ($details as $detail) {
            $result = Db::getInstance()->executeS(
                'SELECT *
FROM ' . _DB_PREFIX_ . 'order_upload
WHERE id_order_detail = ' . $detail['id_order_detail']
            );
            foreach ($result as $item) {
                $product_attribute_string = array();
                $url = str_replace(_PS_UPLOAD_DIR_, '', $item['file_path']);
                if (Tools::file_exists_no_cache(_PS_UPLOAD_DIR_ . $url)) {
                    $product = new Product($item['id_product']);
                    $attributtes = Product::getAttributesParams($item['id_product'], $item['id_product_attribute']);
                    $product_attribute_string[] = $product->name[$this->context->language->id];
                    foreach ($attributtes as $attributte) {
                        $product_attribute_string[] = $attributte['group'] . ': ' . $attributte['name'];
                    }
                    $files[implode(
                        ", ",
                        $product_attribute_string
                    )]['ufiles'][$item['id']]['name'] = $item['file_name'];
                    $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['size'] = filesize(
                        $item['file_path']
                    );
                    $files[implode(", ", $product_attribute_string)]['ufiles'][$item['id']]['url'] = '/upload/' . $url;
                }
            }
        }
        return $files;
    }
}
