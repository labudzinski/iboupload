<div class="panel">
    <div class="panel-heading">
        <i class="icon-cogs"></i> Help
    </div>


    <div class="form-wrapper">
        <ul class="nav nav-pills">
            <li>
                <a class="toolbar_btn" href="https://bitbucket.org/labudzinski/iboupload/issues" target="_blank">
                    <i class="process-icon-help"></i>
                    <div>{l s='Issues' mod='iboupload'}</div>
                </a>
            </li>
            <li>
                <a class="toolbar_btn" href="https://bitbucket.org/labudzinski/iboupload/issues/new" target="_blank">
                    <i class="process-icon-help-new"></i>
                    <div>{l s='Create issue' mod='iboupload'}</div>
                </a>
            </li>
        </ul>

    </div>

</div>