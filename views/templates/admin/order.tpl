{* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt.
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*}
<div class="tab-pane" id="iboupload">
	<div class="panel-heading">
		<i class="icon-money"></i> {l s='Included Files' mod='iboupload'}
	</div>
	
		<div class="table-responsive">
			<table class="table">
				<thead>
					<tr>
						<th><span class="title_box ">{l s='Product' mod='iboupload'}</span></th>
						<th><span class="title_box ">{l s='Name' mod='iboupload'}</span></th>
						<th><span class="title_box ">{l s='Size' mod='iboupload'}</span></th>
					</tr>
				</thead>
				<tbody>
				{if $uproducts}
				{foreach from=$uproducts key='product_name' item='uproduct'}
					{foreach from=$uproduct.ufiles item='ufile' name='i'}
						<tr>
							{if $smarty.foreach.i.index == 0}
								<td rowspan="{$uproduct.ufiles|count}">{$product_name|strval}</td>
							{/if}
							<td>
								<a href="{$ufile.url|strval}" target="_blank">{$ufile.name|strval}</a>
							</td>
							<td>
								{$ufile.size|intval}kb
							</td>
						</tr>
					{/foreach}
				{/foreach}
				{else}
					<tr>
						<td colspan="5" class="list-empty">
							<div class="list-empty-msg">
								<i class="icon-warning-sign list-empty-icon"></i>
								{l s='No files' mod='iboupload'}
							</div>
						</td>
					</tr>
				{/if}
				</tbody>
			</table>
			{if $uproducts}
				<form method="post" action="/modules/iboupload/download.php">
					<input type="hidden" name="id_order" value="{$id_order|intval}">
				<button class="btn btn-primary" name="download" type="submit">{l s='Download all' mod='iboupload'}</button>
				</form>
			{/if}
		</div>
</div>