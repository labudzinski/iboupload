{* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt.
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*}


<div id="product-mnumi" class="panel product-tab">
    <h3>{l s='IBO Upload' mod='iboupload'}</h3>
    <input type="hidden" name="submitted_tabs[]" value="ModuleIboupload" />
    {if $combinations}
        <div class="form-group">
            <table id="table-combinations-list" class="table configuration">
                <thead>
                    <tr class="nodrag nodrop">
                        <th class=" left">
                            <span class="title_box">{l s='Attribute' mod='iboupload'}</span>
                        </th>
                        <th class=" left">
                            <span class="title_box">{l s='Index' mod='iboupload'}</span>
                        </th>
                        <th class=" left">
                            <span class="title_box">{l s='EAN-13' mod='iboupload'}</span>
                        </th>
                        <th class=" left">
                            <span class="title_box">{l s='UPC Barcode' mod='iboupload'}</span>
                        </th>
                        <th class=" left">
                            <span class="title_box">{l s='Can upload?' mod='iboupload'}</span>
                        </th>
                        <th class=" left">
                            <span class="title_box">{l s='Multiple upload' mod='iboupload'}</span>
                        </th>
                        <th class=" left">
                            <span class="title_box">{l s='Maximum number of files' mod='iboupload'}</span>
                        </th>
                    </tr>
                </thead>
                <tbody>
                {foreach from=$combinations item='combination'}
                    <tr class="">
                        <td class=" left">
                            {$combination.attributes}
                        </td>
                        <td class=" left">
                            {$combination.reference}
                        </td>
                        <td class=" left">
                            {$combination.ean13}
                        </td>
                        <td class=" left">
                            {$combination.upc}
                        </td>
                        <td class=" left">
                            <input type="hidden" value="0" name="can_upload[{$combination.id_product_attribute}]">
                            <input type="checkbox" value="1" name="can_upload[{$combination.id_product_attribute}]" {if $combination.can_upload}checked{/if}>
                        </td>
                        <td class=" left">
                            <input type="hidden" value="0" name="multiupload[{$combination.id_product_attribute}]">
                            <input type="checkbox" value="1" name="multiupload[{$combination.id_product_attribute}]" {if $combination.multiupload}checked{/if}>
                        </td>
                        <td class=" left">
                            <input type="number" name="maximum_files[{$combination.id_product_attribute}]" {if $combination.maximum_files}value="{$combination.maximum_files|escape:'htmlall':'UTF-8'}"{/if}>
                        </td>
                    </tr>
                {/foreach}
                </tbody>
            </table>
        </div>
    {else}
        <div class="form-group">
            <label class="control-label col-lg-3">
                {l s='Can upload?' mod='iboupload'}
            </label>
            <div class="col-lg-1">
                <input type="checkbox" value="1" name="can_upload" {if $can_upload}checked{/if}>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-3">
                {l s='Multiple upload' mod='iboupload'}
            </label>
            <div class="col-lg-1">
                <input type="checkbox" value="1" name="multiupload" {if $multiupload}checked{/if}>
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-lg-3">
                {l s='Maximum number of files' mod='iboupload'}
            </label>
            <div class="col-lg-1">
                <input type="number" name="maximum_files" {if $maximum_files}value="{$maximum_files|escape:'htmlall':'UTF-8'}"{/if}>
            </div>
        </div>
    {/if}
    <div class="panel-footer">
        <a href="{$link->getAdminLink('AdminProducts')|escape:'htmlall':'UTF-8'}" class="btn btn-default"><i class="process-icon-cancel"></i> {l s='Cancel' mod='iboupload'}</a>
        <button type="submit" name="submitAddproduct" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save' mod='iboupload'}</button>
        <button type="submit" name="submitAddproductAndStay" class="btn btn-default pull-right"><i class="process-icon-save"></i> {l s='Save and stay' mod='iboupload'}</button>
    </div>
</div>
