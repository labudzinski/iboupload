{* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt.
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*}

<input type="hidden" name="submitted_tabs[]" value="ModuleIboupload" />
<h3>{l s='IBO Upload' mod='iboupload'}</h3>
<table>
	<tbody>
		<tr>
			<td class="col-left"><label>{l s='Can upload?' mod='iboupload'}</label></td>
			<td><input type="checkbox" value="1" name="can_upload" {if $can_upload}checked{/if}></td>
		</tr>
		<tr>
			<td class="col-left"><label>{l s='Multiple upload' mod='iboupload'}</label></td>
			<td><input type="checkbox" value="1" name="multiupload" {if $multiupload}checked{/if}></td>
		</tr>
		<tr>
			<td class="col-left"><label>{l s='Maximum number of files' mod='iboupload'}</label></td>
			<td><input type="text" name="maximum_files" {if $maximum_files}value="{$maximum_files|escape:'htmlall':'UTF-8'}"{/if}></td>
		</tr>
		
	</tbody>
</table>