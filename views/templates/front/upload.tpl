{* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt.
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*}

{capture name=path}
	<a href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}">
		{l s='My account' mod='iboupload'}
	</a>
	<span class="navigation-pipe">{$navigationPipe|htmlall}</span>
	<span class="navigation_page">{l s='Uploaded files' mod='iboupload'}</span>
{/capture}

<h1 class="page-heading">{l s='Uploaded files' mod='iboupload'}</h1>

<div class="row addresses-lists">
	<div class="col-xs-12 col-sm-6 col-lg-4">
		
	</div>
</div>