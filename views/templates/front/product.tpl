{* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt.
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*}

<p class="no-print">
<h4>{l s='This product requires upload files' mod='iboupload'}</h4>
</p>
<p class="no-print">
    <button class="btn btn-warning" id="pickfiles">{l s='Upload files' mod='iboupload'}</button>
    <div id="filelist-container">
        <div id="filelist">
    	</div>
    </div>
	<div id="filebox" style="display:none;"></div>
	{literal}
	<script type="text/javascript">
	var uploader = new plupload.Uploader({
		runtimes : 'html5,flash,silverlight,html4',
		browse_button : 'pickfiles', // you can pass in id...
		container: document.getElementById('filebox'),
		url : "/modules/iboupload/iboupload-ajax.php?action=upload&id_product="
            + {/literal}{$id_product|intval}{literal},
		flash_swf_url : '/js/Moxie.swf',
		silverlight_xap_url : '/js/Moxie.xap',
		dragdrop : true,
		chunk_size: '400kb',
		max_retries: 3,
		max_file_count: {/literal}{$maximum_files|intval}{literal},
		unique_names: {/literal}{$unique_names|strval}{literal},
		multi_selection: {/literal}{if $multi_selection}{$multi_selection|strval}{else}false{/if}{literal},
		filters : {
			max_file_size : '{/literal}{$ibo_max_file_size|strval}{literal}kb',
			{/literal}{if $ibo_allow}{literal}
			mime_types: {/literal}{$ibo_allow|strval|urldecode}{literal}
			{/literal}{/if}{literal}
		},

		init: {
			Init: function(up) {
				$('#filelist').html(null);
				{/literal}
				{if $files}
					{foreach from=$files item=file}
						{literal}
						var file = new plupload.File("{/literal}{$file.id|strval}{literal}",
                                "{/literal}{$file.name|strval}{literal}",
                                {/literal}{$file.size|strval}{literal});
				            file.percent = 100;
				            file.id = "{/literal}{$file.id|strval}{literal}";
				            file.loaded = {/literal}{$file.size|strval}{literal};
				            file.status = plupload.DONE;
				            file.size = {/literal}{$file.size|strval}{literal};
				            file.name = "{/literal}{$file.name|strval}{literal}";
				        uploader.addFile(file, file.name);
						{/literal}
					{/foreach}
				{/if}
				{literal}
			},
			FilesAdded: function(up, files) {
                var max = uploader.settings.max_file_count;
                $.each(files, function(i, file) {
					var html ='<div id="' + file.id + '" class="progressWrapper">' +
    					'<div class="progressContainer">' +
            				'<div class="progressBarWrapper">' +
            					'<div class="progressBar"></div>' +
            				'</div>' +
            				'<div class="contentWrapper">' +
            					'<a class="actionText btn btn-info" role="remove"><i class="icon-remove"></i></a>' +
            					'<div class="title">' +
            					   '<div class="name">' + file.id + '</div>' +
            					   '<div class="status"><b>' + plupload.formatSize(file.size) + '</b></div>' +
            					'</div>' +
            				'</div>' +
            			'</div>' +
            			'<span class="value"></span>' +
            		'</div>';
					$('#filelist').append(html);

					$('#uploadfiles').css('display', 'initial');
				});
				if (files.length > 0) {
					$('#add_to_cart button').attr("disabled", false);
				}
				up.start();
			},
			UploadProgress: function(up, file) {
				$('#filelist #' + file.id + ' .status b').html(file.percent + '%');
				$('#filelist #' + file.id + ' .progressBar').css({'width':file.percent + '%'});
			},
			FileUploaded: function(up, file, info) {

				$('#filelist #' + file.id + ' .title .name').html(file.id);
				$('#filelist #' + file.id + ' .title .status b').html(plupload.formatSize(file.size));
				$('#' + file.id + ' a[role="remove"]').first().click(function(e) {
					e.preventDefault();
                    remove(up, file);
				});
				if (up.files.length > 0) {
					$('#add_to_cart button').attr("disabled", false);
				}
            },
		}
	});uploader.init();
	$('#add_to_cart button').attr("disabled", "disabled");
	$(document).on('click', '#filelist a[role="remove"]', function(e){
		e.preventDefault();
		var data = $(this).parent().parent().parent();
		var id = data.attr('id');
        remove(uploader, uploader.getFile(id));
	});
    function remove(up, file)
    {
        $.post( '/modules/iboupload/iboupload-ajax.php?action=remove&file=' + file.id
                + '&id_product=' + {/literal}{$id_product|intval}{literal}, function( data ) {
            if($.parseJSON(data) == "removed" )
            {
                up.removeFile(file);
                $('#' + file.id).next("br").remove();
                $('#' + file.id).remove();
                if (up.files.length == 0) {
                    $('#uploadfiles').css('display', 'none');
                    $('#add_to_cart button').attr("disabled", "disabled");
                }
            }
        });
    }

	$('#add_to_cart button').click(function() {
		$.each(uploader.files, function (i, file) {
		    if (file) {
		        uploader.removeFile(file);
		        uploader.splice();
		        uploader.refresh();
		        $('#filelist').html(null);
		    }
		});
	});
	</script>
	{/literal}
</p>