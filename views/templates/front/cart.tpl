{* 2014 Dominik Labudzinski
*
* NOTICE OF LICENSE
*
* Przedmiotem licencji są programy IBO, które są całkowitą własnością AT-DesignDominik Labudzinski.
* Są one chroniony przez prawo polskie (Ustawa o Prawie Autorskim i Prawach Pokrewnych z 23 lutego 1994r.
* (Dz.U. 1994, nr 24 poz.83 z pózn. zm.). oraz międzynarodowe postanowienia o ochronie własności intelektualnych.
* Przez instalowanie, kopiowanie bądź korzystanie z produktów firmy AT-DesignDominik Labudzinski
* użytkownik zgadza się przestrzegać warunki licencji.
* Pełny tekst warunków gwarancji i licencji zawiera produkt.
*
*  @author    Dominik Labudzinski <dominik@labudzinski.com>
*  @copyright 2014 Dominik Labudzinski
*  @license   Commercial
*}

{if $uproducts}

<h2 class="page-heading">{l s='Included Files' mod='iboupload'}</h2>
<table class="table table-bordered" id="cart_summary">
	<thead>
		<tr>
			<th class="cart_product first_item">{l s='Product' mod='iboupload'}</th>
			<th class="cart_product item">{l s='Name' mod='iboupload'}</th>
			<th class="cart_product item">{l s='Size' mod='iboupload'}</th>
		</tr>
	</thead>
	<tbody>
    	{foreach from=$uproducts key='product_name' item='uproduct'}
    		{foreach from=$uproduct.ufiles item='ufile' name='i'}
    			<tr class="cart_item">
    				{if $smarty.foreach.i.index == 0}
    					<td rowspan="{$uproduct.ufiles|count}">{$product_name|strval}</td>
    				{/if}
    				<td>
    					<a href="{$ufile.url|strval}" target="_blank">{$ufile.name|strval}</a>
    				</td>
    				<td>
    					{$ufile.size|intval}kb
    				</td>
    			</tr>
    		{/foreach}
    	{/foreach}																												
	</tbody>
</table>
{else}
	<div style="display:none!important;">
		{l s='Included Files' mod='iboupload'}
	</div>

{/if}